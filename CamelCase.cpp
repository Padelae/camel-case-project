/*
Created by Paul Adelae in Visual Studio in C++
Date Created: 12/23/2021
*/

#include <iostream>
#include <string>
using namespace std;
void camelCase(string &text); //Takes in a string and returns a new string in camel case
bool isLetter(char); //returns true if char is a letter
bool isUpper(char); //returns true if char is uppercase
bool isSpace(char);
int main()
{
    string text;
    cout << "Enter a string you want to camel-ify, 0 to stop"<<endl;
    while (text != "0") {
        getline(cin, text);
        camelCase(text);
        cout << "Camel String: " << text << endl;
    }
    return 0;
}
void camelCase(string &text) {
    char letter;
    char prevLetter;
    for (int i = 1; i < text.length(); i++) {
        letter = text[i];
        prevLetter = text[i - 1];
        //If first index is a letter, makes uppercase
        if (i == 1 and isLetter(prevLetter)) {
            if (!isUpper(prevLetter)) {
                prevLetter -= 32;
                text[0] = prevLetter;
            }
            
        }

        //If the previous character is a space, current letter is made uppercase
        if (isSpace(prevLetter)) {
            if (!isUpper(letter)) {
                letter -= 32;
                text[i] = letter;
            }
        }
        //if current index in string is a letter and prev index is also a letter, the
        //current letter is made lower case
        if (isLetter(letter) and isLetter(prevLetter)) {
            if (isUpper(letter)) {
                letter += 32;
                text[i] = letter;
            }
        }
    }
    
}
bool isSpace(char letter) {
    return letter == 32 ? true : false;
}
bool isUpper(char letter) {
    //if letter falls btwn ascii values of A and Z return true
    //ascii value of A is 65, ascii val of Z is 90
    return 65 <= letter and letter<= 90 ? true : false;
}
bool isLetter(char letter) {
    //if letter falls btwn ascii values of A and Z or a and z return true
    //A = 65, a = 97
     return (65 <= letter and letter <= 90) or (97 <= letter and letter<= 122) ? true : false;
}

